<?php

/**
 * @file
 * Install file for the Inform module.
 */

/**
 * Implementation of hook_requirements().
 */
function inform_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    if (class_exists('SoapClient')) {
      $requirements['soap'] = array(
        'value' => t('Installed'),
        'severity' => REQUIREMENT_OK,
        'description' => t('The Inform module relies on the PHP soap extension which is installed.')
      );
    }
    else {
      $requirements['soap'] = array(
        'value' => t('Not installed'),
        'severity' => REQUIREMENT_ERROR,
        'description' => t('The Inform module relies on the PHP soap extension which is not installed.')
      );
    }
    $requirements['soap']['title'] = t('SOAP library');
  }

  return $requirements;
}


/**
 * Implementation of hook_schema().
 */
function inform_schema() {
  $schema = array();
  
  $schema['inform_topics'] = array(
    'description' => 'Stores topics, entities and industries from the Inform API.',
    'fields' => array(
      'id' => array(
        'description' => 'Primary identifier for an Inform topic.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The name of the topic as received from the Inform API.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'display_name' => array(
        'description' => 'The editable name of the topic, overrides name if present.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'The type of topic - topic, entity or industry.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );

  $schema['inform_topics_nodes'] = array(
    'description' => 'Pivot table to join nodes with their corresponding Inform topics.',
    'fields' => array(
      'inform_topic_id' => array(
        'description' => 'Primary identifier for an Inform topic',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => 'The primary identifier for a node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'score' => array(
        'description' => 'The score of this topic in relation to this node.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'high_score' => array(
        'description' => 'Whether or not this node\'s score exceeds the minimum required.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('inform_topic_id', 'nid'),
    'indexes' => array(
      'nid' => array('nid'),
      'high_score_inform_topic_id' => array('high_score', 'inform_topic_id'),
    ),
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 */
function inform_install() {
  drupal_install_schema('inform');
}

/**
 * Implementation of hook_uninstall().
 */
function inform_uninstall() {
  drupal_uninstall_schema('inform');
  variable_del('inform_progress_changed');
  variable_del('inform_progress_nid');
  variable_del('inform_process_on_update');
  variable_del('inform_minimum_score');
  variable_del('inform_batch_size');
  variable_del('inform_content_types');
  variable_del('inform_url');
  variable_del('inform_itoken');
}
