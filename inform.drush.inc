<?php


/**
 * @file
 * Drush support for the inform module
 */

/**
 * Implementation of hook_drush_command().
 */
function inform_drush_command() {
  $items = array();

  $items['inform-cron'] = array(
    'callback' => '_inform_run_batches',
    'description' => dt("Processes a batch of nodes using the Inform API."),
  );  
  $items['inform-process'] = array(
    'callback' => 'inform_drush_process_node',
    'description' => dt("Processes a single node using the Inform API."),
    'arguments' => array(
      'nid' => dt('The numerical node id of the node to be processed.'),
    ),
    'options' => array(
      'echo-traffic' => dt('A debugging option that prints out what is sent to Inform and what is received.'),
    ),
    'examples' => array(
      'drush inform-process 1234',
      'drush inform-process 2345 --echo-traffic',
    ),
  );
  $items['inform-nodeinfo'] = array(
    'callback' => 'inform_drush_nodeinfo',
    'description' => dt("List Inform topics for a node."),
  );
    
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function inform_drush_help($section) {
  switch ($section) {
    case 'drush:inform-cron':
      return dt("Run Inform cron hook.");
      break;
    case 'drush:inform-process':
      return dt("Processes a single node using the inform module.  A numerical node id is given as an argument, and there is an optional flag, --echo-traffic, for debugging the communication with the Inform service.");
      break;
    case 'drush:inform-nodeinfo':
      return dt("Lists Inform topics for a node.");
      break;
  }
}

/**
 * Callback for drush command `inform process %nid`.
 */
function inform_drush_process_node($nid) {
  if ($node = node_load($nid)) {
    if (drush_get_option('echo-traffic', FALSE)) {
      $debug_str = inform_process_node($node, TRUE);
      drush_print(dt('DEBUG: !str', array('!str' => $debug_str)));
    }
    else {
      inform_process_node($node);
    }
    
    $total = count(inform_get_topics_for_node($nid));
    drush_log(dt("nid:!nid ('!title') has !total topics", array('!nid' => $nid, '!title' => $node->title, '!total' => $total)), 'success');
  }
  else {
    drush_log(dt("Unable to load node with nid: !nid", array('!nid' => $nid)), 'error');
  }
}

/**
 * Callback for drush command `inform nodeinfo %nid`.
 */
function inform_drush_nodeinfo($nid) {
  if ($node = node_load($nid)) {
    $output = '';
    
    // Get all topics associated with a node.
    $topics = inform_get_topics_for_node($nid);
    
    // Display information about each topic.
    foreach ($topics as $topic) {
      // Inform topic score.
      $output .= "[SCORE: " . str_pad($topic->score, '3', ' ', STR_PAD_LEFT) . "] ";
      
      // Inform topic type.
      switch ($topic->type) {
        case INFORM_TERM_INDUSTRY:
          $output .= 'INDUSTRY: ';
          break;
        case INFORM_TERM_ENTITY:
          $output .= 'ENTITY: ';
          break;
        case INFORM_TERM_TOPIC:
          $output .= 'TOPIC: ';
          break;
      }
      
      $output .= $topic->name . PHP_EOL;
    }
    
    drush_log(dt("Topics for '!title'\n\n!output", array('!title' => $node->title, '!output' => $output)), 'success');
  }
  else {
    drush_log(dt("Unable to load node with nid: !nid", array('!nid' => $nid)), 'error');  
  }
}
